package pl.michal.client;

import java.io.Serializable;
import java.sql.Timestamp;

public class Message implements Serializable{

	private String message;
	private Timestamp timestamp;
	static final long serialVersionUID = 1L;
	
	public Message(String message, Long time)
	{
		this(message);
		this.timestamp = new Timestamp(time);
	}
	
	public Message(String message)
	{
		this.message = message;
	}
	
	public String getMessage()
	{
		return message;
	}
	
	public Timestamp getTimestamp()
	{
		return timestamp;
	}
}
