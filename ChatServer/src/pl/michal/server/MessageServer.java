package pl.michal.server;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Collections;
import java.util.LinkedList;

public class MessageServer {

	
	private LinkedList<Socket> socketList;
	private ServerSocket serverSocket;
	private Thread connectionListener;
	
	public MessageServer()
	{
		socketList = (LinkedList<Socket>) Collections.synchronizedCollection(new LinkedList<Socket>());
		try {
			serverSocket = new ServerSocket(55500);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void start()
	{
		connectionListener = new Thread(){
			@Override
			public void run()
			{
				while(true)
				{
					try {
						Socket socket = serverSocket.accept();
						socketList.add(socket);
						System.out.println("New connection from " + 
								socket.getRemoteSocketAddress().toString() +
								"at port " + socket.getPort());
						InputStream inpStream = socket.getInputStream();
						ObjectInputStream objInpStream = new ObjectInputStream(inpStream);
						
					}
					catch(IOException e) {
						e.printStackTrace();
					}	
				}
			}
		};
		connectionListener.run();
		while(socketList.isEmpty());//test code from now on
		try {
			InputStream inpStr = socketList.peekFirst().getInputStream();
			ObjectInputStream objInpStr = new ObjectInputStream(inpStr);
			Message message = (Message) objInpStr.readObject();
			System.out.println(message.getMessage());
			objInpStr.close();
			socketList.getFirst().close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	
	
	
	
}
